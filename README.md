# About Hours Admin SDK

## Installation
- composer require locally/hours-admin-sdk

## Usage
Depending on what API you need, you can simply instantiate the needed class:

**Available classes:**

  - LocationApi
  - BusinessApi
  - CrawlerApi
  - HolidayApi
  - SundayShoppingApi

**Example:**

```php
$locationApi = new LocationApi('API_TOKEN');
```
   - Argument 1: Api token that needs to correspond to the chosen enviroment (production/staging). Default environment is production.   
   - Argument 2 (optional): Boolean that sets the correct api url to production OR staging server. True = production (default), false = staging.
   
Then you can simply execute any of the available methods in the class, examples:
     
```php
$locationApi->index()               // return all the locations
$locationApi->show($hash)           // return location with matching $hash and highest version
$locationApi->update($arrayOfData)  // update location based on the passed array and return
$locationApi->store($arrayOfData)   // store new location based on the passed array and return
```
