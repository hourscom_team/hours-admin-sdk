<?php

namespace HoursAdminSdk;

use GuzzleHttp\Client as Guzzle;

class BaseApi
{
    const URLS = [
        'production' => 'https://admin.hours.com/api/',
        'staging'    => 'https://devbackend.openingstijden.org/api/',
        'local'      => 'http://admin-hours.test:81/api/',
    ];

    protected Guzzle $guzzle;
    protected string $baseUrl;

    public function __construct(string $apiToken, string $env = 'production')
    {
        $this->baseUrl = static::URLS[$env] ?? static::URLS['production'];
        $this->guzzle  = new Guzzle([
            'headers' => [
                'Authorization' => 'Bearer ' . $apiToken,
                'Accept'        => 'application/json',
            ]
        ]);
    }

    public function setUrl(string $url): self
    {
        $this->baseUrl = $url;

        return $this;
    }

    public function getUrl(string $env = 'production'): string
    {
        return static::URLS[$env];
    }

    public function get(string $resourceName, array $data = [], bool $associativeArray = true)
    {
        return $this->request('GET', $resourceName, $data, $associativeArray);
    }

    public function post(string $resourceName, array $data = [], bool $associativeArray = true)
    {
        return $this->request('POST', $resourceName, $data, $associativeArray);
    }

    public function patch(string $resourceName, array $data = [], bool $associativeArray = true)
    {
        return $this->request('PATCH', $resourceName, $data, $associativeArray);
    }

    public function put(string $resourceName, array $data = [], bool $associativeArray = true)
    {
        return $this->request('PUT', $resourceName, $data, $associativeArray);
    }

    public function delete(string $resourceName, array $data = [], bool $associativeArray = true)
    {
        return $this->request('DELETE', $resourceName, $data, $associativeArray);
    }

    public function request(string $method, string $resourceName, array $data = [], bool $associativeArray = true)
    {
        $bodyType = $method === 'GET' ? 'query' : 'json';
        $uri      = $this->baseUrl . $resourceName;

        $response = $this->guzzle->request($method, $uri, [
            $bodyType => $data
        ]);

        return json_decode((string)$response->getBody(), $associativeArray);
    }
}