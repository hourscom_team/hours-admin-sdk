<?php

namespace HoursAdminSdk;

class BusinessApi extends BaseApi
{
    protected string $resourceName = 'businesses';

    /**
     * Get all businesses
     *
     * GET /api/businesses
     *
     * Provide additional query data for search/filter by providing GET parameters in the $data array
     * as simple associative array
     *
     * Example:
     * ['name' => 'BusinessName', 'country' => 'nl']
     */
    public function index(array $data = []): array
    {
        return $this->get($this->resourceName, $data);
    }

    /**
     * Get business by ID
     *
     * GET /api/businesses/{id}
     */
    public function show(int $id): array
    {
        return $this->get($this->resourceName . '/' . $id);
    }

    /**
     * Update business by ID
     *
     * PUT /api/businesses/{id}
     */
    public function update(int $id, array $data): array
    {
        return $this->put($this->resourceName . '/' . $id, $data);
    }

    /**
     * Store new business
     *
     * POST /api/businesses
     */
    public function store(array $data): array
    {
        return $this->post('/api/kc', $data);
    }

    /**
     * Delete business
     *
     * DELETE /api/businesses/{id}
     */
    public function destroy(int $id): array
    {
        return $this->delete($this->resourceName . '/' . $id);
    }
}
