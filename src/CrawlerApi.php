<?php

namespace HoursAdminSdk;

class CrawlerApi extends BaseApi
{
    protected string $resourceName = 'crawlers';

    /**
     * Get all crawlers
     *
     * GET /api/crawlers
     *
     * Provide additional query data for search/filter by providing GET parameters in the $data array
     * as an associative array
     * ['country' => 'be', 'status' => 8]
     */
    public function index(array $data = []): array
    {
        return $this->get($this->resourceName, $data);
    }

    /**
     * Get crawler by ID
     *
     * GET /api/crawlers/{id}
     */
    public function show(int $id): array
    {
        return $this->get($this->resourceName. '/' . $id);
    }

    /**
     * Update crawler by ID
     *
     * PUT /api/crawlers/{id}
     */
    public function update(int $id, array $data): array
    {
        return $this->put($this->resourceName . '/' . $id, $data);
    }
}
