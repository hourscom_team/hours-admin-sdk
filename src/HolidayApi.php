<?php

namespace HoursAdminSdk;

class HolidayApi extends BaseApi
{
    protected string $resourceName = 'holidays';

    /**
     * Get all holidays
     *
     * GET /api/holidays
     */
    public function index(array $data = []): array
    {
        return $this->get($this->resourceName, $data);
    }

    /**
     * Get holiday by ID
     *
     * GET /api/holidays/{id}
     */
    public function show(int $id): array
    {
        return $this->get($this->resourceName . '/' . $id);
    }
}
