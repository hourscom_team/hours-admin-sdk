<?php

namespace HoursAdminSdk;

class PublishedLocationApi extends BaseApi
{
    protected string $resourceName = 'published/locations';

    /**
     * Get all locations
     *
     * GET /api/published/locations
     *
     * Provide additional query data for search/filter by providing GET parameters in the $data array
     * as simple associative array
     *
     * Example:
     * ['business_id' => 5648, 'country_code' => 'nl', 'status' => 'bankrupt', 'city_id' => '1']
     *
     * Or search via LAT and LNG with radius
     * ['lat' => '423424243324', 'lng' => '4233434324', 'radius' => '50']
     *
     */
    public function index(array $data = []): array
    {
        return $this->get($this->resourceName, $data);
    }

    /**
     * Get location by hash
     *
     * GET /api/published/locations/{hashSlugOrId}
     */
    public function show($hashSlugId, array $data = []): array
    {
        return $this->get($this->resourceName . '/' . $hashSlugId, $data);
    }

    /**
     * Store new location
     */
    public function store(array $data): array
    {
        return $this->post('/api/kc', $data);
    }
}
