<?php

namespace HoursAdminSdk;

class SundayShoppingApi extends BaseApi
{
    protected string$resourceName = 'sundayshoppings';

    /**
     * Get Municipality
     */
    public function municipality(string $gm): array
    {
        return $this->get($this->resourceName . '/municipality/' . $gm);
    }

    /**
     * Get District
     */
    public function district(string $wk): array
    {
        return $this->get($this->resourceName . '/district/' . $wk);
    }

    /**
     * Get Neighborhood
     */
    public function neighborhood(string $bu): array
    {
        return $this->get($this->resourceName . '/neighborhood/' . $bu);
    }
}
